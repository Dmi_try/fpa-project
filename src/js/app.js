import 'jquery';
import 'owl.carousel';


$(document).ready(function () {

    // carousel
    $('.js-carousel').owlCarousel({
        items: 1,
        loop: true,
        center: true,
        mouseDrag: false,
        touchDrag: false,
        nav: true,
        dots: false,
        margin: 0,
        smartSpeed: 450,
    });

});


















