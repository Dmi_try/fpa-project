let gMap = {
    init: function () {
        this.initCache();
        this.initMap();
    },

    initCache: function() {
        this.$body = $('body');
    },

    initMap: function () {
        var coordinates = { lat: 55.7419225, lng: 37.6439117 },
            zoom = 15,
            image = '/img/ic-pin.svg',

            map = new google.maps.Map(document.getElementById('map'), {
                center: coordinates,
                zoom: zoom,
                disableDefaultUI: true,
                scrollwheel: false
            });
        
            marker = new google.maps.Marker({
                position: coordinates,
                map: map,
                icon: image
            });

        $.getJSON("../map-style.json", function (data) {
            map.setOptions({styles: data});
        });

    }
};

$(document).ready(function() {
    gMap.init();
});